const router = require('koa-router')()
const getData = require('../controllers/index')
const finddata = require('../validator/auth')

router.get('/hospitaldata/list',finddata, getData)

module.exports = router