const { hostiptalCheck } = require('../db/querys/listdata')

const finddata = async(ctx, next) => {
    const hid = ctx.request.query.hid    
    if(!hid){
        ctx.status = 401
        ctx.body = { message : "Please Enter Hospital Id"}
    }    
    else{
        const checkdata = await hostiptalCheck(+hid)
        if(checkdata){
            return next()
        }else{
            ctx.status = 401
            ctx.body = { message : "Please Enter Valid Hospital Id"}
        }
    }
}

module.exports = finddata